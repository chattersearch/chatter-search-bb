# Chatter Search Web [![build status](https://gitlab.com/chattersearch/chatter-search-bb/badges/master/build.svg)](https://gitlab.com/chattersearch/chatter-search-bb/commits/master)

> Web client for Chatter Search API.

[Demo.](https://chatter-search-bb.surge.sh/)

Backbone/Requirejs/SASS based app. Linted with standard.
